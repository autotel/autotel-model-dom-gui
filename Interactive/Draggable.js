import Vector2 from "../utils/Vector2"
import Component from "../GuiComponents/Component";
import anyElementToDomElement from "../utils/anyElementToDomElement";
import Mouse from "./Mouse";

const VectorTypedef = require("../utils/Vector2");

/**
 * @typedef {VectorTypedef.MiniVector} MiniVector
 */


/**
 * @typedef {Node} NodeWithClassList
 * @property {Set<string>} classList
 * @exports NodeWithClassList
 */



/**
 * @class Draggable
 * 
 * thing that can be dragged. It does not implement actual updating of position,
 * as it doesn't assume the object to have certain properties for position or 
 * certain render methods. The user must implement by using dragCallback function
 * override*/
class Draggable {
    /**
    *  @constructor 
    *  @param {HTMLElement|SVGElement|Component} element 
    * */
    constructor(element) {
        const mouse = Mouse.get();

        let domElement = anyElementToDomElement(element);

        const position = new Vector2();
        const dragStartPosition = position.clone();

        domElement.addEventListener("mouseenter", (evt) => {
            mouse.isHovering = this;
        });
        domElement.addEventListener("touchstart", (evt) => {
            mouse.isHovering = this;
        },{
            passive:true
        });

        domElement.addEventListener("mouseleave", (evt) => {
            if (!mouse.selected.has(this)) {
                domElement.classList.remove("active");
            }
            mouse.isHovering = false;
        });
        domElement.addEventListener("touchend", (evt) => {
            if (!mouse.selected.has(this)) {
                domElement.classList.remove("active");
            }
            mouse.isHovering = false;
        });

        /** do not override */
        this._drag = () => {

            position.set(dragStartPosition);
            position.add(mouse.delta);

            this.dragCallback(mouse);
            const positionChangedArgs = {
                x: position.x,
                y: position.y,
                delta: mouse.delta,
                localDragOffset: dragStartPosition,
                start: {
                    x: dragStartPosition.x,
                    y: dragStartPosition.y,
                }
            };
            this.positionChanged(positionChangedArgs);
            positionChangedCallbacks.forEach((cb)=>cb(positionChangedArgs));
        }
        
        this._dragStart = (mouse) => {

            dragStartPosition.set(position);

            domElement.classList.add("active");
            this.dragStartCallback(mouse);
        }
        this._dragEnd = (...p) => {
            domElement.classList.remove("active");
            this.dragEndCallback(...p);
        }

        /**
         * @callback MouseActionCallback 
         * @param {Mouse} mouse
         */
        const dragCallbacks = [];
        const dragStartCallbacks = [];
        const dragEndCallbacks = [];
        const positionChangedCallbacks = [];
        
        /** @param {MouseActionCallback} callback */
        this.onDrag = (callback) => {
            dragCallbacks.push(callback);
        }
        /** @param {MouseActionCallback} callback */
        this.onDragStart = (callback) => {
            dragStartCallbacks.push(callback);
        }
        /** @param {MouseActionCallback} callback */
        this.onDragEnd = (callback) => {
            dragEndCallbacks.push(callback);
        }
        /** @param {DragPosition} callback */
        this.onPositionChanged = (callback) => {
            positionChangedCallbacks.push(callback);
        }

        this.dragCallback = (mouse) => {
            dragCallbacks.forEach((callback)=>callback(mouse));
        }
        this.dragStartCallback = (mouse) => {
            dragStartCallbacks.forEach((callback)=>callback(mouse));
        }
        this.dragEndCallback = (mouse) => {
            dragEndCallbacks.forEach((callback)=>callback(mouse));
        }

        /**
         * @typedef {Object} DragPosition
         * @property {number} DragPosition.x
         * @property {number} DragPosition.y
         * @property {MiniVector} DragPosition.start
         * @property {MiniVector} DragPosition.delta
         * @property {MiniVector} DragPosition.localDragOffset
         **/

        /** @param {DragPosition} newPosition */
        this.positionChanged = (newPosition) => {}

        this.setPosition = (newPosition, callback = true) => {
            position.set(newPosition);
            if (callback) this.positionChanged(newPosition);
        }

        domElement.classList.add("draggable");
    }
}
/** @param {Node | Component | HTMLElement | SVGElement} canvas */
Draggable.setCanvas = (canvas = document) => {
    const mouse = Mouse.get();
    mouse.setCanvas(canvas);
}
export default Draggable;