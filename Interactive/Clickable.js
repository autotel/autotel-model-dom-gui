import Vector2 from "../utils/Vector2"
import Component from "../GuiComponents/Component";
import anyElementToDomElement from "../utils/anyElementToDomElement";
import Mouse from "./Mouse";
const VectorTypedef = require("../utils/Vector2");

/**
 * @typedef {VectorTypedef.MiniVector} MiniVector
 */


/**
 * @typedef {Node} NodeWithClassList
 * @property {Set<string>} classList
 * @exports NodeWithClassList
 */



/**
 * @class Clickable
 * 
 * thing that can be dragged. It does not implement actual updating of position,
 * as it doesn't assume the object to have certain properties for position or 
 * certain render methods. The user must implement by using dragCallback function
 * override
 *  @constructor 
 *  @param {HTMLElement|SVGElement|Component} element */
function Clickable(element) {
    const mouse = Mouse.get();
    let domElement = anyElementToDomElement(element);

    let clickStartedHere = false;

    this.mouseDownCallback = (mouse) => { }
    this.clickCallback = (mouse) => { }
    this.mouseUpCallback = (mouse) => { }
    this.mouseEnterCallback = (mouse) => { }
    this.mouseLeaveCallback = (mouse) => { }


    mouse.onUp(() => {
        clickStartedHere = false;
    });

    domElement.addEventListener('mousedown', () => {
        clickStartedHere = true;
        this.mouseDownCallback(mouse);
    });
    domElement.addEventListener('mouseup', () => {
        this.mouseUpCallback(mouse);
        if (clickStartedHere) {
            this.clickCallback(mouse);
        }
    });
    domElement.addEventListener('mouseenter', () => {
        this.mouseEnterCallback(mouse);
    });
    domElement.addEventListener('mouseleave', () => {
        this.mouseLeaveCallback(mouse);
    });

    domElement.classList.add("clickable");
}

/** @param {Node} canvas */
Clickable.setCanvas = (canvas = document) => {
    const mouse = Mouse.get();
    mouse.setCanvas(canvas);
}
export default Clickable;