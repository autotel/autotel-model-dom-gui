import Vector2 from "../utils/Vector2";
import Draggable from "./Draggable";
import Hoverable from "./Hoverable";
import anyElementToDomElement from "../utils/anyElementToDomElement";
import Component from "../GuiComponents/Component";

/** @typedef {import("../utils/Vector2").MiniVector} MiniVector*/

/**
 * @callback mouseCallback 
 * @param {Mouse} mouse
 */

class Mouse extends Vector2 {
    constructor() {
        super();
        /** @type {boolean} */
        this.pressed = false;
        /** @type {Set<Draggable|Hoverable>} */
        this.selected = new Set();
        /** @type {false|Draggable|Hoverable} */
        this.isHovering = false;
        /** @type {Vector2} */
        this.dragStartPosition = new Vector2();
        /** @type {Vector2} */
        this.delta = new Vector2();

        this.controlKeyWhenPressed = false;


        /** @type {Array<mouseCallback>} */
        const moveCallbacks = [];
        /** @type {Array<mouseCallback>} */
        const downCallbacks = [];
        /** @type {Array<mouseCallback>} */
        const upCallbacks = [];

        /** @param {mouseCallback} callback */
        this.onMove = (callback) => moveCallbacks.push(callback);
        /** @param {mouseCallback} callback */
        this.onDown = (callback) => downCallbacks.push(callback);
        /** @param {mouseCallback} callback */
        this.onUp = (callback) => upCallbacks.push(callback);
        /** @param {HTMLElement | SVGElement | Component} clientCanvas */
        this.setCanvas = (clientCanvas) => {
            const canvas = anyElementToDomElement(clientCanvas);
            const pointerMoveListener = (evt) => {
                // @ts-ignore
                this.x = evt.clientX;
                // @ts-ignore
                this.y = evt.clientY;
                //translate touch events 
                if (evt.touches && evt.touches.length) {
                    this.x = evt.touches[0].clientX;
                    this.y = evt.touches[0].clientY;
                }

                this.delta = Vector2.sub(this, this.dragStartPosition);
                if (this.pressed) {
                    if (this.selected.size) {
                        evt.preventDefault();
                        this.selected.forEach((draggable) => { draggable._drag(this) });
                    }
                }
                moveCallbacks.forEach((cb) => cb(this));
            }
            canvas.addEventListener("mousemove", pointerMoveListener);
            canvas.addEventListener("touchmove", (evt) => {
                //in touch we cannot hover before clicking,
                //so we need to pick up any hovered element
                //even if the drag action already started
                if (this.isHovering) {
                    if (!this.selected.has(this.isHovering)) {
                        if (this.isHovering._dragStart) {
                            this.isHovering._dragStart(this);
                        }
                        this.selected.add(this.isHovering);
                    }
                }

                pointerMoveListener(evt);
            });

            const pointerDownListener = (evt) => {
                this.controlKeyWhenPressed =  evt.ctrlKey;
                this.pressed = true;
                //@ts-ignore
                this.dragStartPosition.set({ x: evt.clientX, y: evt.clientY });
                // @ts-ignore
                if (evt.button == 0) {
                    //to implement multi element seletion, you would do changes here
                    if (this.isHovering) {
                        evt.preventDefault();
                        this.selected.clear();
                        this.selected.add(this.isHovering);
                        this.selected.forEach((draggable) => {
                            if (draggable._dragStart) {
                                draggable._dragStart(this)
                            }
                        });
                    }
                }

                downCallbacks.forEach((cb) => cb(this));
            }
            canvas.addEventListener("mousedown", pointerDownListener);
            canvas.addEventListener("touchstart", (evt) => {

                //translate touch position
                if (evt.touches.length) {
                    this.x = evt.touches[0].clientX;
                    this.y = evt.touches[0].clientY;
                }

                pointerDownListener(evt);
            });

            const pointerUpListener = (evt) => {
                this.pressed = false;
                if (this.selected.size) {
                    evt.preventDefault();
                    this.selected.forEach((draggable) => {
                        if (draggable._dragEnd) draggable._dragEnd(this);
                    });
                    this.selected.clear();
                }
                upCallbacks.forEach((cb) => cb(this));
            }
            canvas.addEventListener("mouseup", pointerUpListener);
            canvas.addEventListener("touchend", (evt) => {
                //translate touch position
                if (evt.touches.length) {
                    this.x = evt.touches[0].clientX;
                    this.y = evt.touches[0].clientY;
                }

                pointerUpListener(evt);
            });
        }
    }
}

Mouse.instance = undefined;
/** @returns {Mouse} */
Mouse.get = () => {
    if (!Mouse.instance) Mouse.instance = new Mouse();
    return Mouse.instance;
}

export default Mouse;