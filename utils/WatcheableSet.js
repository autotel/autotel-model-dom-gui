
/** 
 * 
 * @typedef {Object} ChangeCallbackArgs
 * @property {any} ChangeCallbackArgs.argument
 * @property {string} ChangeCallbackArgs.action
 */
/**
 * @callback ChangeCallback
 * @param {ChangeCallbackArgs} args
 */

import removeFromArray from "./removeFromArray";

/**
 * @template TYPE
 */
class WatcheableSet {
    /**
     * @param {Array<TYPE>} iterable
     */
    constructor(iterable = []) {
        /** 
         * @type {Set<TYPE>} 
         */
        const set = new Set(iterable);

        /**
         * @type {Array<ChangeCallback>}
         */
        const changeCallbacks = [];
        /**
         * @param {ChangeCallback} changeCallback
         */
        this.onChange = (changeCallback) => {
            changeCallbacks.push(changeCallback);
        }

        this.removeChangeCallback = (callback, throwIfNonexistent = false) => {
            removeFromArray(changeCallbacks, callback, throwIfNonexistent);
        }

        /**
         * @param {TYPE} argument elements to add
         * @returns {WatcheableSet<TYPE>} 
         */
        this.add = (argument) => {
            set.add(argument);
            this.size = set.size;
            changeCallbacks.forEach(cb => cb({ action: "add", argument }));
            return this;
        }

        /**
         * @param {Array<TYPE>} argument elements to add
         * @returns {WatcheableSet<TYPE>} 
         */
        this.addArray = (argument) => {
            argument.forEach(i => set.add(i));
            this.size = set.size;
            changeCallbacks.forEach(cb => cb({ action: "add", argument }));
            return this;
        }

        /**
         * @param {TYPE} argument elements to add
         * @returns {Boolean} 
         */
        this.delete = (argument) => {
            let r = set.delete(argument);
            this.size = set.size;
            changeCallbacks.forEach(cb => cb({ action: "add", argument }));
            return r;
        }

        this.clear = () => {
            set.clear();
            this.size = set.size;
            changeCallbacks.forEach(cb => cb({ action: "clear", argument: null }));
            return this;
        }

        /**
         * @param {TYPE} item
         */
        this.has = (item) => set.has(item);
        this.size = set.size;

        /**
         * @returns {IterableIterator<any,TYPE>} 
         */
        this.entries = () => set.entries();
        this.keys = () => set.keys();
        /**
         * @returns {IterableIterator<TYPE>} 
         */
        this.values = () => set.values();

        /**
         * @returns {Array<TYPE>} 
         */
        this.asArray = () => Array.from(set);

        /**
         * @callback ForEachCallback
         * @param {TYPE} item
         */
        /**
         * @param {ForEachCallback} callback
         */
        this.forEach = (callback) => {
            set.forEach((a) => callback(a));
        }
        /**
         * @param {ForEachCallback} callback
         * @returns {Array<TYPE>} 
         */
        this.map = (callback) => {
            let result = [];
            set.forEach((a) => result.push(callback(a)));
            return result;
        }
        /**
         * @param {ForEachCallback} callback
         * @returns {Array<TYPE>} 
         */
        this.filter = (callback) => {
            let result = [];
            set.forEach((a) => {
                if (callback(a)) result.push();
            });
            return result;
        }
        /**
         * @callback FindCallback
         * @param {TYPE} item
         */
        /**
         * @param {FindCallback} callback
         */
        this.find = (callback) => {
            return [...set.values()].find((a) => callback(a));
        }
    }
}

export default WatcheableSet;