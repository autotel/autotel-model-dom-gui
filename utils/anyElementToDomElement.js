import Component from "../GuiComponents/Component";

/**
 * @param {HTMLElement|SVGElement|Component} element
 * @returns {HTMLElement|SVGElement}
 */
const anyElementToDomElement=(element)=>{
    
    let domElement;
    if(element instanceof Component){
        domElement = element.domElement;
    }else{
        domElement = element;
    }
    return domElement;
}

export default anyElementToDomElement;