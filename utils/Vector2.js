/**
 * @typedef {Object} MiniVector
 * @property {number} [x]
 * @property {number} [y]
 * @export MiniVector
 */

/**
 * @class Vector 2
 * @param {Vector2|MiniVector} options
 */
function Vector2(options = { x: 0, y: 0 }) {
    /** @type {number} */
    this.x = options.x;
    /** @type {number} */
    this.y = options.y;
    /** @param {Vector2|MiniVector} to */
    this.add = (to) => {
        this.x += to.x;
        this.y += to.y;
        return this;
    }
    /** @param {Vector2|MiniVector} to */
    this.sub = (to) => {
        this.x -= to.x;
        this.y -= to.y;
        return this;
    }
    this.clone = () => {
        return new Vector2(this);
    }
    /** @param {Vector2|MiniVector} to */
    this.set = (to) => {
        if (to.x !== undefined) this.x = to.x;
        if (to.y !== undefined) this.y = to.y;
    }
    /** @param {number} angleRadians */
    this.rotate = (angleRadians) => {
        let x = this.x;
        let y = this.y;
        this.x = x * Math.cos(angleRadians) - y * Math.sin(angleRadians)
        this.y = x * Math.sin(angleRadians) + y * Math.cos(angleRadians)
    }
    /** 
     * @param {Vector2|MiniVector} to
     * @returns {number} 
     **/
    this.distance = (to) => Vector2.distance(this, to);
    this.longitude = () => Vector2.longitude(this);
    this.set(options);
}

/** 
 * @param {Vector2|MiniVector} vec1 
 * @param {Vector2|MiniVector} vec2 
 * @returns {number} 
 **/
Vector2.distance = (vec1, vec2) => {
    const diffVec = new Vector2(vec1).sub(vec2);
    return Vector2.longitude(diffVec);
}
/** 
 * @param {Vector2|MiniVector} vec1 
 * @returns {number} 
 **/
Vector2.longitude = (vec1) => {
    return Math.sqrt(vec1.x * vec1.x + vec1.y * vec1.y);
}

/** 
 * @param {Vector2|MiniVector} vec1 
 * @param {Vector2|MiniVector} vec2 
 **/
Vector2.add = (vec1, vec2) => {
    return (new Vector2(vec1)).add(vec2);
}

/** 
 * @param {Vector2|MiniVector} vec1 
 * @param {Vector2|MiniVector} vec2 
 **/
Vector2.sub = (vec1, vec2) => {
    return (new Vector2(vec1)).sub(vec2);
}

/** 
 * @param {Vector2|MiniVector} vec1
 **/
Vector2.clone = (vec1) => {
    return (new Vector2(vec1));
}

/** 
 * @param {Array<number>} arr
 **/
Vector2.fromArray = (arr) => {
    return new Vector2({ x: arr[0], y: arr[1] });
}

export default Vector2;