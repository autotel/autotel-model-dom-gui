const getOrMakeEl = () => {
    let el = document.getElementById("mobile-log");
    if(!el){
        el = document.createElement("div");
        el.setAttribute("id","mobile-log");
        document.body.append(el);
    }
    return el;
}

const mobileLog = (...p) => {
    let str = p.map((subp)=>JSON.stringify(subp)).join();
    let el = getOrMakeEl();
    el.innerHTML=str;
}

export default mobileLog;