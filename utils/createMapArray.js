const createMapArray = (cb,times)=>{
    let ret = [];
    for(let a = 0; a<times; a++){
        ret.push(cb(a));
    }
    return ret;
}

export default createMapArray;