const round = (num,decp = 2) => {
    let m = Math.pow(10,decp);
    return Math.round(num * m) / m;
}
export default round;