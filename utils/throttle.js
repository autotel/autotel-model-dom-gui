const throttle = (callback,time = 1) => {
    return new (function(callback){
        this.timeout = null;
        this.call=()=>{
            if(this.timeout !== null) clearTimeout(this.timeout);
            this.timeout = setTimeout(()=>{
                callback();
                this.timeout=null;
            },time);
        }
    })(callback).call;
}
export default throttle;