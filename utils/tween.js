class Tween {
    constructor(starVal,endVal,duration) {
        let run = false;
        let started = 0;
        /** @type {FrameRequestCallback } */
        const step = (time) => {
            if(!started) started = time;
            let lerp = (time - started) / duration;

            if(lerp >= 1){
                this.stepFunction(endVal,1);
                this.stopFunction();
                this.destroy();
                return;
            }
            let val =  endVal * lerp
                + starVal * (1-lerp);
            
            this.stepFunction(val,lerp);
            
            if (run) {
                requestAnimationFrame(step);
            }
        }

        this.stepFunction = (val,lerp) => { };
        this.stopFunction = () => { };

        this.start = (stepFunction) => {
            if(stepFunction) this.stepFunction=stepFunction;
            run = true;
            requestAnimationFrame(step);
        }
        this.stop = () => {
            run = false;
            this.stopFunction();
            this.destroy();
        }
        this.destroy = () => {
            run=false;
            this.stepFunction = () => {}
            return false;
        }
    }
}
export default Tween;