
/**
 * @template T
 * @param {Array<T>} array array where to search
 * @param {T} element element to look for
 * @param {boolean} throwIfNonexistent throw error if not present
 */
const removeFromArray = (array, element, throwIfNonexistent = true) => {
    const index = array.indexOf(element);
    if (index > -1) {
        array.splice(index, 1)
    } else if (throwIfNonexistent) {
        // console.log({ array, element });
        throw new Error("cannot remove element from array");
    }
}


export default removeFromArray;