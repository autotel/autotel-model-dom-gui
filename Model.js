/**
 * @typedef {Object<string,any>} ChangedParameterList
 */

import removeFromArray from "./utils/removeFromArray";

/**
 * @callback ChangesListener
 * @param {ChangedParameterList} changes an object containing only the changed parameters. Modify this object to alter the changes
 */

/**
 * @callback BeforeChangesListener
 * @param {ChangedParameterList} changes an object containing only the changed parameters. Modify this object to alter the changes
 * @param {Object<string,any>} settings an object containing all the model's properties.
 */

/**
 * @template SETTINGS
 */

class Model {
    /**
     * @param {SETTINGS} settings
     */
    constructor(settings) {
        const beforeChangeListeners = [];
        const changeListeners = [];
        /** @type {SETTINGS}  */
        this.settings = settings;

        /** 
         * this is used to tie parameters together, for example if one setting is 
         * always 2 times other setting.
         * @param {BeforeChangesListener} newCallback
         **/
        this.beforeUpdate = (newCallback) => {
            if (typeof newCallback !== "function")
                throw new Error(`Callback has to be function but it is ${typeof newCallback}`);
            beforeChangeListeners.push(newCallback);
            newCallback(this.settings, this.settings);
        };
        /**
         * interface uses this method to connect changes in model to redraws
         * @param {ChangedParameterList} newCallback
         * */
        this.onUpdate = (newCallback) => {
            if (typeof newCallback !== "function")
                throw new Error(`Callback has to be function but it is ${typeof newCallback}`);
            changeListeners.push(newCallback);
            newCallback(this.settings);
        };

        this.removeUpdateListener = (callback) => {
            if (typeof callback !== "function")
                throw new Error(`Callback has to be function but it is ${typeof callback}`);
            
            removeFromArray(changeListeners,callback);
        }

        /**
         * model uses this method to notify changes to the interface
         * it will not trigger "beforeUpdate" listeners.
         * @param {ChangedParameterList} [changes]
         **/
        this.changed = (changes = {}) => {
            changeListeners.forEach((cb) => { cb(changes); });
        };

        /**
         * change parameter values, triggering onchange listeners
         * @param {ChangedParameterList} [changes]
         **/
        this.set = (changes = {}) => {
            beforeChangeListeners.forEach((cb) => { cb(changes, this.settings); });
            Object.assign(this.settings, changes);
            this.changed(changes);
            return this;
        }

        //get the initial state of the model
        this.triggerInitialState = () => {
            this.set(settings);
        };

        this.destroy = () => {
            beforeChangeListeners.splice(0);
            changeListeners.splice(0);
        }
    }
}
export default Model;