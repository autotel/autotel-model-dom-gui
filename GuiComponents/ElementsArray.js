import Component from "./Component";
import { SVGGroup, SVG } from "./SVGElements";
import { Div, HTMLComponent } from "./DOMElements";
import WatcheableSet from "../utils/WatcheableSet";
import throttle from "../utils/throttle";

class ElementsArray {
    /**
     * @param {typeof SVGElementsArray | typeof DOMElementsArray} ListComponentConstructor
     * @param {SVGElementsArray | DOMElementsArray} groupComponent
     * @param {Object} [componentConstructorParameters]
     * the constructor for the gui component that represents
     * one item in the list.
     **/
    constructor(
        ListComponentConstructor,
        groupComponent,
        componentConstructorParameters = {}
    ) {
        /** 
         * Contains the list of instanced components.
         * In this way it re-utilizes components that 
         * may have been used before.
         */
        const instancesList = [];

        Object.assign(
            componentConstructorParameters,
            {
                parentElement: groupComponent
            }
        );

        this.displayListComponent = (number, properties) => {
            if (!instancesList[number]) {
                instancesList[number] = new ListComponentConstructor(componentConstructorParameters);
                groupComponent.add(instancesList[number]);
            }
            instancesList[number].set(properties);
            instancesList[number].show(properties);
        }
        /** 
         * @param {number} from
         * @param {number} [to]
         */
        this.hideInstances = (from, to) => {
            if (to === undefined) to = instancesList.length;
            for (let index = from; index < to; index++) {
                instancesList[index].hide();
            }
        }

        /** @param {WatcheableSet} watcheableSet*/
        this.setToWatcheableSet = (watcheableSet) => {
            console.log("assign watcheable set");
            const setChangeHandler = throttle(() => {
                console.log("update list");
                this.displayArray(watcheableSet.asArray());
            }, 100);
            watcheableSet.onChange(setChangeHandler);
            this.refresh = setChangeHandler;
        }
        this.refresh = () => {
            console.log("only available if using watcheableSet");
        }
        this.displayArray = (array) => {
            let lastDisplayedValue = 0;
            array.forEach((value, number) => {
                this.displayListComponent(number, value);
                lastDisplayedValue++;
            });
            this.hideInstances(lastDisplayedValue);
        }

        groupComponent.instancesList = instancesList;
        groupComponent.displayArray = this.displayArray;
    }
}

class SVGElementsArray extends SVGGroup {
    constructor(
        ListComponentConstructor,
        componentConstructorParameters = {}
    ) {
        super();
        this.instancesList = [];
        this.displayArray = (array) => { }
        this.arrayController = new ElementsArray(
            ListComponentConstructor,
            this,
            componentConstructorParameters
        );
        this.setToWatcheableSet = this.arrayController.setToWatcheableSet;
        this.refresh = () => this.arrayController.refresh();
    }
}

class DOMElementsArray extends Div {
    constructor(
        ListComponentConstructor,
        componentConstructorParameters = {}
    ) {
        super();
        this.instancesList = [];
        this.displayArray = (array) => { }
        this.arrayController = new ElementsArray(
            ListComponentConstructor,
            this,
            componentConstructorParameters
        );
        this.setToWatcheableSet = this.arrayController.setToWatcheableSet;
        this.refresh = () => this.arrayController.refresh();
    }
}

class ListElement {
    constructor(properties) {
        this.hide = () => { }
        this.show = () => { }
        this.set = () => { }
    }
}

class DOMListElement extends Div {
    constructor(properties) {
        // console.log({properties});
        super();

        this.hide = () => { }
        this.show = () => { }
        this.set = (changes) => { this.setAttributes(changes); }

        if (properties && properties.parentElement) {
            /** @type {DOMElementsArray} */
            const parentElement = properties.parentElement;
            this.hidden = true;

            /** meant to be overriden, but these defaults might suffice. */
            this.hide = () => {
                // console.log("auto-hide",this.domElement);
                if (this.hidden) return;
                parentElement.remove(this);
            }

            this.show = () => {
                // console.log("auto-show",this.domElement);
                if (!this.hidden) return;
                parentElement.add(this);
            }

            this.set = (settings) => { }
        }

    }
}

class SVGListElement extends SVG {
    constructor(properties) {
        super();

        this.hide = () => { }
        this.show = () => { }
        this.set = (changes) => { this.setAttributes(changes); }

        if (properties && properties.parentElement) {
            /** @type {SVGElementsArray} */
            const parentElement = properties.parentElement;
            this.hidden = true;
            /** meant to be overriden, but these defaults might suffice. */
            this.hide = () => {
                if (this.hidden) return;
                parentElement.remove(this);
            }

            this.show = () => {
                if (!this.hidden) return;
                parentElement.add(this);
            }

            this.set = (settings) => { }

        }
    }
}

export {
    ListElement,
    DOMListElement,
    SVGListElement,
    SVGElementsArray,
    DOMElementsArray,
}