import Model from "../Model"
import { SVGGroup, Text, Path, Rectangle } from "../GuiComponents/SVGElements";
import Draggable from "../Interactive/Draggable";
import Clickable from "../Interactive/Clickable";

class Toggle extends SVGGroup {
    constructor(userOptions) {
        const options = {
            x: 0, y: 0,
            width: 20,
            name: "toggle",
            class: "toggle",
            min: false, max: false,
        };

        Object.assign(options, userOptions);

        super({
            x: options.x,
            y: options.y,
            width: options.width,
            height: options.width,
            class: options.class
        });

        let nameText = new Text({
            x: 0,
            y: options.width + 5
        });

        this.add(nameText);

        let buttonShape = new Rectangle();
        let valueShape = new Rectangle();
        this.add(valueShape);
        this.add(buttonShape);
        valueShape.setAttributes({ "fill": "none" });
        valueShape.addClass("knob-value-arc");

        const remakeValueShape = () => {
            valueShape.attributes.width = options.width - 6;
            valueShape.attributes.height = options.width - 6;

            valueShape.attributes.x = 3 - options.width / 2;
            if (this.value) {
                valueShape.attributes.y = 3 - options.width;
            } else {
                valueShape.attributes.y = 3;
            }

            valueShape.update();
        }

        const remakePath = () => {
            buttonShape.attributes.width = options.width;
            buttonShape.attributes.height = options.width * 2;

            buttonShape.attributes.x = -options.width / 2;
            buttonShape.attributes.y = -options.width;

            nameText.attributes["text-anchor"] = "middle";
            nameText.attributes.y = options.width * 1.5;

            nameText.update();
            buttonShape.update();
            remakeValueShape();
        }

        remakePath();

        const changeCallbacks = [];

        const clickable = new Clickable(buttonShape.domElement);

        clickable.mouseUpCallback = () => {
            this.changeValue(this.value ? false : true);
        }

        this.value = false;

        /** @param {Function} cb */
        this.onChange = (cb) => {
            changeCallbacks.push(cb);
        }
        const handleChanged = (changes) => changeCallbacks.map((cb) => cb(changes));

        this.updateGraphic = () => {
            nameText.setAttributes({ "text": options.name });
            remakeValueShape();
        }

        this.changeValue = (to) => {
            this.value = to;
            this.updateGraphic();

            handleChanged({ value: to });
        }

        /** 
         * @param {Model} model
         * @param {string} parameterName
         */
        this.setToModelParameter = (model, parameterName) => {
            let propertyObject = {};
            propertyObject = model.settings;
            if (!options.name) {
                options.name = parameterName;
            }
            this.value = propertyObject[parameterName];

            this.onChange(({ value }) => {
                propertyObject[parameterName] = value;

                model.set(propertyObject);
            });

            model.onUpdate((changes) => {
                if (changes[parameterName]) {
                    this.value = changes[parameterName];
                    this.updateGraphic();
                }
            });
            this.updateGraphic();
        }

        if (options.model && options.parameter) {
            this.setToModelParameter(
                options.model, options.parameterName
            );
        }
    }
}

export default Toggle;