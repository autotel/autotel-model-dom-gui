

/**
 * @typedef {Object<String,any>} ComponentOptions
 * @property {number} [x]
 * @property {number} [y]
 */

/**
 * Defines a drawable object which can be attached to a Sprite or a Group
 */
class Component {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = { x: 0, y: 0 }) {
        /** @type {SVGElement|HTMLElement|undefined} */
        this.domElement=undefined;
        this.attributes = {};
        this.appliedAttributes = {};
        this.update = () => {
            if (!this.domElement)
                return console.warn("this.domElement is", this.domElement);
            //apply what has been modified to the dom element
            //it also keeps track of modified attributes to prevent redundant changes
            Object.keys(this.attributes).map((attrName) => {
                try{
                    const attr = this.attributes[attrName];
                    const appliedAttr = this.appliedAttributes[attrName];
                    if (attr !== appliedAttr) {
                        this.domElement.setAttribute(attrName, attr);
                        this.attributes[attrName] = attr;
                        this.appliedAttributes[attrName] = attr;
                    }
                }catch(e){
                    console.error(e);
                    console.log(attrName,this.attributes);
                }
            });
        };
        this.addClass = (...classes) => {
            let classString = classes.join(" ").split(/\s+/g);
            classString.forEach((classString)=>{
                this.domElement.classList.add(classString);
            });
        }
        this.removeClass = (...classes) => {
            let classString = classes.join(" ").split(/\s+/g);
            classString.forEach((classString)=>{
                this.domElement.classList.remove(classString);
            });
        }
        /*
        (\.set\()("[^"]*"),([^\)]*)
        $1{$2:$3}
        */
        this.setAttributes = (newAtrrs) => {
            Object.assign(this.attributes,newAtrrs);
            this.update();
        };

        this.destroy = () => {
            this.domElement.remove();
            Object.keys(this).forEach((key)=>{
                delete this[key]
            });
        }
        this.detach = () => {
            const parent = this.domElement.parentElement;
            if(parent){
                parent.removeChild(this.domElement);
            }
            return parent;
        }
        Object.assign(this.attributes, myOptions);
    }
}

export default Component;