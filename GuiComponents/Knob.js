
import { SVGGroup, Text, Path } from "./SVGElements";
import round from "../utils/round";
import Draggable from "../Interactive/Draggable";
import Model from "../Model";

/**
 * @typedef {"integer"|"linear"|"frequency"|"gain"|"channelvol"|"integer"|"periodseconds"} DeltaCurveName
 **/

/**
 * @type {Object<DeltaCurveName,Function>} */
const deltaCurves = {
    periodseconds:(deltaval)=> {
        const newVal = Math.pow(deltaval/10,2)*10 * Math.sign(deltaval);
        return newVal;
    },
    integer:(deltaval)=> {
        const newVal = Math.round(deltaval * 20);
        return newVal;
    },
    frequency:(deltaval)=>{
        deltaval*=100;
        return Math.pow(Math.abs(deltaval),2)*Math.sign(deltaval);
    },
    gain:(deltaval)=>(deltaval*3),
    channelvol:(deltaval)=>deltaval*5,
    linear:(deltaval)=>deltaval,
}

class Knob extends SVGGroup{
    constructor(userOptions){
        const options = {
            x: 0, y:0,
            radius:20,
            /** @type {string|false} */
            name:false,
            class:"knob",
            /** @type {number|false} */
            min:false,
            /** @type {number|false} */
            max:false,
            /** @type {DeltaCurveName} */
            deltaCurve:"linear",
        };

        Object.assign(options,userOptions);

        super({
            x:options.x,
            y:options.y,
            width: options.radius,
            height:options.radius,
            class:options.class,
        });

        let nameText = new Text({
            x:0,
            y: options.radius * 1.4,
            "style":"font-size:"+options.radius * 0.5+"px",
            'text-anchor':'middle'
        });
        let valueText = new Text({
            x:0,
            y: options.radius * 1.9,
            "style":"font-size:"+options.radius * 0.5+"px",
            'text-anchor':'middle'
        });

        if(options.name){
            this.add(nameText);
        }
        this.add(valueText);


        let knobShape = new Path();
        let valueShape = new Path();
        this.add(valueShape);
        this.add(knobShape);
        valueShape.setAttributes({"fill":"none"});
        valueShape.addClass("knob-value-arc");

        const remakeValueShape=()=>{
            let maxValue = options.max?options.max:1;
            // console.log(maxValue);
            // if(!maxValue) throw new Error("maxvalue"+maxValue);
            let endPortion = this.value / maxValue;
            if(endPortion>1) endPortion=1;
            //there's no good reason for using an arc.
            let maxCorners = 54;
            let lastPoint = [];
            let pathString = "M 0 0";

            let corners = maxCorners * endPortion;

            for(let corner = 0; corner<corners; corner++){
                let nowPoint=[
                    (Math.cos(Math.PI * 2 * corner/maxCorners) * options.radius) || 0,
                    (Math.sin(Math.PI * 2 * corner/maxCorners) * options.radius) || 0,
                ];
                if(corner > 0){
                    pathString += `L ${nowPoint.join()} `;
                }else{
                    pathString += `M ${nowPoint.join()}`;
                }
                lastPoint=nowPoint;
            }

            //add that last one bit
            let nowPoint=[
                (Math.cos(Math.PI * 2 * endPortion) * options.radius) || 0,
                (Math.sin(Math.PI * 2 * endPortion) * options.radius) || 0,
            ];
            pathString += `L ${nowPoint.join()} `

            valueShape.setAttributes({"d":pathString});
        }
        
        const remakePath=()=>{
            let corners = 7;
            let lastPoint = [];
            let pathString = "";

            for(let corner = 0; corner<corners; corner++){
                let nowPoint=[
                    (Math.cos(Math.PI * 2 * corner/corners) * options.radius * 0.6) || 0,
                    (Math.sin(Math.PI * 2 * corner/corners) * options.radius * 0.6) || 0,
                ];
                if(corner > 0){
                    pathString += `L ${lastPoint.join()} ${nowPoint.join()} `
                }else{
                    pathString += `M ${nowPoint.join()}`;
                }
                lastPoint=nowPoint;
            }
            
            pathString += `z`;
            if(options.min !==false && options.max !==false){
                //knob direction indicator
                pathString += `M ${options.radius * 0.6},${0}`;
                pathString += `Q ${options.radius * 0.6},${0} ${0},${0}`
            }
            knobShape.setAttributes({"d":pathString});
            remakeValueShape();
        }

        remakePath();

        const changeCallbacks=[];
        
        this.step=1/300;
        
        let pixValueOnDragStart;

        const distanceToValue=(pixels)=> pixels * this.step;
        const valueToPixels=(value)=> value / this.step ;
        const getAngle=()=>{
            let rpv = this.step * 300;
            if(options.min!==false && options.max!==false){
                let range = options.max - options.min;
                rpv = 1/range;
            }
            return (rpv * this.value * 360)|0;
        }


        const draggable = new Draggable(knobShape.domElement);

        draggable.dragStartCallback=()=>{
            pixValueOnDragStart = valueToPixels(this.value);
            if(isNaN(pixValueOnDragStart)) pixValueOnDragStart=0;
            this.addClass("active");
        }

        draggable.dragEndCallback=()=>{
            this.removeClass("active");
        }

        draggable.positionChanged=(newPosition)=>{
            //choose the lengthiest coord to define delta
            let theDistance = -newPosition.delta.y;
            let valueDelta = distanceToValue(theDistance);
            let newValue = deltaCurves[options.deltaCurve](valueDelta);
            
            newValue+=distanceToValue(pixValueOnDragStart);

            if(options.min !== false){
                if(newValue < options.min) newValue = options.min;
            }
            if(options.max !== false){
                if(newValue > options.max) newValue = options.max;
            }
            this.changeValue(
                newValue
            );
        }

        this.value=0;
        /** @param {Function} cb */
        this.onChange=(cb)=>{
            changeCallbacks.push(cb);
        }

        const handleChanged=(changes)=> changeCallbacks.forEach((cb)=>cb(changes));
        
        this.updateGraphic=()=>{
            knobShape.setAttributes({"transform":`rotate(${getAngle()})`});

            let dispVal = round(this.value,2);
            if(isNaN(dispVal)) dispVal = 0;
            valueText.setAttributes({"text":"~"+(dispVal)});

            if(options.min!==false&&options.max!==false){
                remakeValueShape();
            }
        }

        this.changeValue=(to)=>{
            this.value=to;
            this.updateGraphic();
            handleChanged({value:to});
        }
        
        //TODO: this is specific to fields project, thus needs to be removed and extended in that project.
        /** 
         * @param {Model} model
         * @param {string} parameterName
         */
        this.setToModelParameter=(model,parameterName)=>{
            
            let propertyObject = {};
            
            if(!options.name){
                options.name=parameterName;
            }

            nameText.setAttributes({"text":options.name});
            this.add(nameText);
            this.value=propertyObject[parameterName];

            
            this.onChange(({value})=>{
                propertyObject[parameterName] = value;
                model.set(propertyObject);
            });

            model.onUpdate((changes)=>{
                if(changes[parameterName]){
                    this.value=changes[parameterName];
                    this.updateGraphic();
                }
            });

            switch (parameterName){
                case "frequency":
                    this.setDeltaCurve("frequency");
                    this.setMinMax(0,22000);
                break;
                case "order":
                    this.setDeltaCurve("integer");
                    this.setMinMax(0,10);
                break;

                case "levela":
                case "levelb":
                case "levelc":
                case "leveld":
                case "time":
                case "length":
                    this.setMinMax(0,5);
                case "gain":
                    this.setDeltaCurve("gain");
                default:
                    this.setDeltaCurve("linear");
                break;
            }

            this.updateGraphic();
        }

        this.setMinMax=(min,max)=>{
            if(max<=min) console.warn("max<=min",min,max);
            options.min=min;
            options.max=max;
            remakePath();
            return this;
        }
        /**
         * @param {DeltaCurveName} deltaCurve
         **/
        this.setDeltaCurve=(deltaCurve)=>{
            options.deltaCurve=deltaCurve;
            return this;
        }
        
        if(options.deltaCurve){
            this.setDeltaCurve(options.deltaCurve);
        }

        if(options.model && options.parameter){
            this.setToModelParameter(
                options.model,options.parameter
            );
        }
    }
}

export default Knob;