import Component from "./Component";
import Model from "../Model";

/**
 * @typedef {import("./Component").ComponentOptions} ComponentOptions
 */
/**
 * @typedef {ComponentOptions} CircleOptions
 * @param {number} [radius]
 */
class Circle extends Component {
    /**
     * @param {CircleOptions} myOptions
     **/
    constructor(myOptions = {
        cx: 0, cy: 0, r: 50
    }) {
        super(myOptions);
        // Component.call(this, myOptions);
        this.domElement = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
        this.update();
    }
}

/**
 * @typedef {ComponentOptions} PathOptions
 */
class Path extends Component {
    /**
     * @param {PathOptions} myOptions
     **/
    constructor(myOptions = {
        d: ``
    }) {
        // Component.call(this, myOptions);
        super(myOptions);
        this.domElement = document.createElementNS("http://www.w3.org/2000/svg", 'path');
        this.update();

        /** @param {Array<import("../utils/Vector2").MiniVector>} coords */
        this.setCoordinatesList = (coords) => {
            if (!coords.length) {
                console.log({ coords, s:""});
                return this.setAttributes({ d: "" });
            }
            let poss = coords.map(({ x, y }) => (x + "," + y));
            let str = "M" + poss.join(" L");
            this.setAttributes({ d: str });
        }

    }

}

/**
 * @typedef {ComponentOptions} RectangleOptions
 * @property {string} [fill]
 * @property {number} [width]
 * @property {number} [height]
 */
class Rectangle extends Component {
    /**
     * @param {RectangleOptions} myOptions
     **/
    constructor(myOptions = {
        x: 0, y: 0, width: 100, height: 100
    }) {
        super(myOptions);
        this.domElement = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
        this.update();
    }
}

/**
 * @typedef {ComponentOptions} LineOptions
 */
class Line extends Component {
    /**
     * @param {LineOptions} myOptions
     **/
    constructor(myOptions = {
        x1: 0, y1: 80, x2: 100, y2: 20
    }) {
        super(myOptions);
        // Component.call(this, myOptions);
        this.domElement = document.createElementNS("http://www.w3.org/2000/svg", 'line');
        this.update();
    }
}

/**
 * @typedef {ComponentOptions} SVGOptions
 */
class SVG extends Component {
    /** @param {SVGOptions} myOptions */
    constructor(myOptions = {
        x: 0, y: 0,
    }) {
        super(myOptions);
        // Component.call(this, myOptions);
        this.domElement = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
        /** @param {Array<Component>} elems */
        this.add = (...elems) => {
            elems.forEach((elem) => {
                if (!(elem.domElement instanceof SVGElement)) throw new Error("you can only add SVG elements to SVG group.");
                this.domElement.appendChild(elem.domElement);
            });
            return this;
        };
        /** @param {Array<Component>} elems */
        this.remove = (...elems) => {
            elems.forEach((elem) => {
                this.domElement.removeChild(elem.domElement);
            });
            return this;

        }
        this.update();
    }
}

class SVGGroup extends SVG { }


class SVGCanvas extends SVG {
    constructor(...p) {
        super(...p);
        // const element = document.createElementNS("http://www.w3.org/2000/svg",'svg');
        const element = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
        this.element = element;

        // element.setAttributes('viewBox',"0 0 1000 1000");
        // element.setAttribute('width',"100%");
        // element.setAttribute('height',"10000px");

        document.body.appendChild(element);


        const size = this.size = {
            width: 0,
            height: 0,
            onChange: ({ }) => { },
        };

        const scroll = this.scroll = {
            top: 0,
            left: 0
        }

        const sizeModel = new Model(size);
        const scrollModel = new Model(scroll);

        size.onChange = sizeModel.onUpdate;

        const recalcSize = () => {
            sizeModel.set({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        }

        const recalcScroll = () => {
            scrollModel.set({
                top: window.scrollX,
                left: window.scrollY,
            });
        }

        window.addEventListener("resize", recalcSize);
        window.addEventListener("scroll", recalcScroll);
        window.addEventListener("DOMContentLoaded", () => {
            recalcSize();
            recalcScroll();
        });

    }
}

/**
 * @typedef {ComponentOptions} TextOptions
 * @property {"middle"|"left"|"right"} 'text-anchor'
 */
class Text extends Component {

    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {
        x: 0, y: 0, text: "---"
    }) {
        super(myOptions);
        // Component.call(this, myOptions);
        this.domElement = document.createElementNS("http://www.w3.org/2000/svg", 'text');

        const superUpdate = this.update;
        this.update = () => {
            this.domElement.innerHTML = this.attributes.text;
            superUpdate();
        };
        this.update();
    }
}
/**
 * @typedef {ComponentOptions} TextOptions
 * @property {"middle"|"left"|"right"} 'text-anchor'
 */
class BoxedText extends SVG {

    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {
        x: 0, y: 0, text: "---"
    }) {
        super(myOptions);

        const textEl = new Text();
        const box = new Rectangle();
        this.add(box, textEl);

        this.addClass("boxed-text");
        const superUpdate = this.update;
        this.update = () => {
            if (this.attributes.text) {
                textEl.attributes.text = this.attributes.text;
                delete this.attributes.text;
            }

            textEl.update();

            setTimeout(() => {
                const SVGRect = textEl.domElement.getBBox();
                box.attributes.x = SVGRect.x;
                box.attributes.y = SVGRect.y;
                box.attributes.width = SVGRect.width;
                box.attributes.height = SVGRect.height;
                box.update();
            }, 300);

            superUpdate();
        };
        //todo: fix this bug
        setTimeout(() => {
            this.update();
        }, 10);
    }
}



export {
    Circle,
    Path,
    Line,
    Rectangle,
    SVG, SVGGroup, SVGCanvas,
    Text,
    BoxedText,
}
