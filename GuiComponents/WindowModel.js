import Model from "../Model";

class WindowModel extends Model {
    constructor() {

        const settings = {
            width: 100,
            height: 100,
            scroll: {
                x: 0, y: 0,
            },
        }

        super();

        if (WindowModel.instance) {
            console.warn("you have more than instance of WindowModel, which is wasteful. Use WindowModel.get instead of new WindowModel.");
        } else {
            WindowModel.instance = this;
        }

        //redundant assignation but helps the IDE
        this.settings = settings;

        window.addEventListener('scroll',()=>{
            this.set({
                scroll:{
                    x:window.scrollX,
                    y:window.scrollY,
                }
            });
        });
        
        window.addEventListener('resize',()=>{
            this.set({
                width:window.innerWidth,
                height:window.innerHeight 
            });
        });

        window.addEventListener('load',()=>{
            this.set({
                x:window.scrollX,
                y:window.scrollY,
                width:window.innerWidth,
                height:window.innerHeight 
            });
        });

        this.set({
            x:window.scrollX,
            y:window.scrollY,
            width:window.innerWidth,
            height:window.innerHeight 
        });
    };
}
/**
 * @type {WindowModel|false} 
 **/
WindowModel.instance = false;

WindowModel.get = () => {
    return WindowModel.instance || (new WindowModel());
}

export default WindowModel;