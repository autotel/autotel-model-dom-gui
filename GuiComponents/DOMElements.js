import Component from "./Component";
/**
 * @typedef {import("./Component").ComponentOptions} ComponentOptions
 */

class HTMLComponent extends Component {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions);
        this.appliedAttributes.innerHTML = "";
        /** @param {Array<Component>} elems */
        this.add = (...elems) => {
            elems.forEach((elem) => {
                if (
                    !(
                        elem.domElement instanceof HTMLElement
                        || elem.domElement instanceof SVGSVGElement
                    )
                ) throw new Error("you can only add HTMLElements to HTMLElements.");
                this.domElement.appendChild(elem.domElement);
            });
            return this;
        };
        const superUpdate = this.update;
        this.update = () => {
            superUpdate();
            this.domElement.innerHTML = this.appliedAttributes.innerHTML;
        };

        this.remove = (elem) => {
            this.domElement.removeChild(elem.domElement);
        }
    }
}

class Div extends HTMLComponent {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions);
        this.domElement = document.createElement("div");

        this.update();
    }
}

class Ul extends HTMLComponent {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions);
        this.domElement = document.createElement("ul");

        this.update();
    }
}

class P extends HTMLComponent {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions);
        this.domElement = document.createElement("p");

        const superUpdate = this.update;
        this.setText = (text) => this.setAttributes({ text });
        this.update = () => {
            this.attributes.innerHTML = this.attributes.text;
            superUpdate();
        };
        this.update();
    }
}
class H1 extends HTMLComponent {
    /** @param {TextOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions);
        this.domElement = document.createElement("H1");

        const superUpdate = this.update;

        this.update = () => {
            this.attributes.innerHTML = this.attributes.text;
            superUpdate();
        };
        this.update();
    }
}

class H2 extends HTMLComponent {
    /** @param {TextOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions);
        this.domElement = document.createElement("H2");

        const superUpdate = this.update;

        this.update = () => {
            this.attributes.innerHTML = this.attributes.text;
            superUpdate();
        };
        this.update();
    }
}

class H3 extends HTMLComponent {
    /** @param {TextOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions);
        this.domElement = document.createElement("H3");

        const superUpdate = this.update;

        this.update = () => {
            this.attributes.innerHTML = this.attributes.text;
            superUpdate();
        };
        this.update();
    }
}

class H4 extends HTMLComponent {
    /** @param {TextOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions);
        this.domElement = document.createElement("H4");

        const superUpdate = this.update;

        this.update = () => {
            this.attributes.innerHTML = this.attributes.text;
            superUpdate();
        };
        this.update();
    }
}


/**
 * @callback updateListener
 * @param {string} newValue
 */
class Field extends HTMLComponent {
    constructor(myOptions = {}, domElement) {


        super(myOptions);

        /** @type {HTMLInputElement|HTMLTextAreaElement} */
        this.domElement = domElement;

        /** @type {Array <updateListener> */
        const updateListeners = [];
        let inputListenerAdded = false;
        const valueChangeHandler = () => {
            updateListeners.forEach(cb => cb(this.attributes.value));
        }

        this.onInput = (callback) => {
            this.domElement.addEventListener('input', callback);
        }

        /** @param {updateListener} callback */
        this.onValueChange = (callback) => {
            if (!inputListenerAdded) {
                this.domElement.addEventListener('input', valueChangeHandler);
                inputListenerAdded = true;
            }
            updateListeners.push(callback);
        }

        this.onBlur = (callback) => {
            this.domElement.addEventListener('blur', callback);
        }

        const superUpdate = this.update;

        this.update = () => {
            this.attributes.value = this.attributes.text;
            superUpdate();
            valueChangeHandler();
        };

        this.update();
    }
}

class Textarea extends Field {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions, document.createElement("textarea"));

        const superUpdate = this.update;
        this.update = () => {
            this.domElement.innerHTML = this.attributes.text;
            superUpdate();
        };
    }
}

class TextField extends Field {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions, document.createElement("input"));
        this.domElement.setAttribute("type", "text");
        this.setText = (text) => {
            this.setAttributes({ text, value: text });
        }

    }
}
class NumberField extends Field {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {

        super(myOptions, document.createElement("input"));
        this.domElement.setAttribute("type", "number");

    }
}
class CheckboxField extends Field {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions, document.createElement("input"));
        this.domElement.setAttribute("type", "checkbox");
    }
}

/**
 * @typedef {Object} DropdownOptionElementSetParameter
 * @property {String|number|boolean} DropdownOptionElementSetParameter.value
 * @property {String|number|boolean} DropdownOptionElementSetParameter.text
 **/

class DropdownOptionElement extends HTMLComponent {
    /** @param {DropdownField} parentElement */
    constructor(parentElement) {
        super();

        this.domElement = document.createElement("option");

        // const superUpdate = this.update;
        // this.update = () => {
        //     superUpdate();
        // };

        /** @param {DropdownOptionElementSetParameter} changes  */
        this.set = (changes) => {
            if (!changes.text === undefined) changes.text = changes.value;
            if (!changes.value === undefined) changes.value = changes.text;
            this.setAttributes(changes);
        }

        this.hidden = true;

        this.hide = () => {
            if (this.hidden) return;
            parentElement.remove(this);
        }

        this.show = () => {
            if (!this.hidden) return;
            if (this.domElement.parentNode) return (console.log("already appd."));
            parentElement.add(this);
        }

        this.update();
    }
}
class DropdownField extends Field {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions, document.createElement("select"));

        /** @type {Array<DropdownOptionElement>} */
        this.optionsList = [];

        /** @type {Array<DropdownOptionElementSetParameter>} */
        this.options = [];
        const superUpdate = this.update;

        let optionAddThrottleTimeout;
        /** 
         * @param {string} text
         * @param {string} [value]
         */
        this.addOption = (text, value) => {
            if (!value) value = text;
            this.options.push({ text, value });

            if (optionAddThrottleTimeout) clearInterval(optionAddThrottleTimeout);
            optionAddThrottleTimeout = setTimeout(() => this.update(), 5);
        }

        this.update = () => {
            let removeFrom = 0;
            this.options.forEach((userOption, index) => {
                if (!this.optionsList[index]) {
                    this.optionsList[index] = new DropdownOptionElement(this);
                }
                this.optionsList[index].set(userOption);
                this.optionsList[index].show();
                removeFrom = index;
            });

            this.optionsList.slice(removeFrom).map((optionEl) => {
                optionEl.hide();
            });
            this.domElement.normalize();
            superUpdate();
        };

    }
}

// class MathFormulaField extends Div {
//     constructor(myOptions){
//         super(myOptions);
//         const inputText = this.inputText = new TextField();
//         const rendered = this.rendered = new Div();
//         this.add(inputText,rendered);

//         const changeHandlers = [];
//         this.onChange = handler => changeHandlers.push(handler);
//         const handleChange = (...p) => changeHandlers.forEach(h=>h(...p));

//         inputText.onInput(()=>{
//             try{
//                 // katex.render(
//                 //     inputText.domElement.value,
//                 //     rendered.domElement,
//                 // );
//                 handleChange(inputText.domElement.value);
//             }catch(e){
//                 inputText.addClass("error");
//                 console.warn("math parse error:",e);
//             }
//         });
//     }
// }

class UploadField extends Field {
    /** @param {ComponentOptions} myOptions */
    constructor(myOptions = {}) {
        super(myOptions, document.createElement("input"));
        this.domElement.setAttributes("type", "file");
    }
}

class TabButton extends Div {
    constructor(settings) {
        super();
        const text = new P(settings);
        this.add(text);
        this.addClass("tab button");
    }
};

class TabsContainer extends Div {
    constructor(myOptions) {
        super(myOptions);
        /**
         * @typedef TabInst
         * @property {Component} TabInst.element
         * @property {TabButton} TabInst.button
         * @property {string} TabInst.name
         */

        /** @type {Array<TabInst>} */
        const tabs = this.tabs = [];
        this.tabsContainer = new Div();
        this.contentsContainer = new Div();
        this.addClass("tab-container");
        this.contentsContainer.addClass("tab-contents-container");
        this.tabsContainer.addClass("tab-buttons-container");

        this.add(this.tabsContainer, this.contentsContainer);

        /** @param {TabInst} theTab*/
        this.openTab = (theTab) => {
            tabs.forEach((tab) => {
                tab.button.removeClass("active");
            });
            theTab.button.addClass("active");
            this.contentsContainer.domElement.innerHTML = "";
            this.contentsContainer.add(theTab.element);
        };
        /** 
         * @param {Component} element 
         * @param {string} name 
         * */
        this.addTab = (element, name) => {
            const button = new TabButton({ text: name });
            /** @type {TabInst} */
            let newTab = {
                button, name, element
            }

            button.domElement.addEventListener("click", () => {
                this.openTab(newTab);
            });

            this.tabsContainer.add(button);

            tabs.push(newTab);
            return newTab;
        }

        this.openTabNamed = name => {
            let found = tabs.filter(tab => tab.name == name)[0];
            return this.openTab(found);
        }

        this.removeTab = tab => {
            let index = tabs.indexOf(tab);
            if (index > -1) {
                tab.button.destroy();
                tab.element.destroy();
                tabs.splice(index, 1);
            }
        }
        this.removeTabNamed = name => {
            let found = tabs.filter(tab => tab.name == name)[0];
            return this.removeTab(found);
        }
    }
}

export {
    Component,
    HTMLComponent,
    Div,
    Ul,
    P,
    H1, H2, H3, H4,
    Textarea,
    TextField,
    NumberField,
    CheckboxField,
    UploadField,
    DropdownField,
    // MathFormulaField,
    TabsContainer,
}