import Model from "../Model"
import { SVGGroup, Text, Path, Rectangle } from "./SVGElements";
import Draggable from "../Interactive/Draggable";
import Clickable from "../Interactive/Clickable";


class Button extends SVGGroup {
    constructor(userOptions) {
        const options = {};
        Object.assign(options, {
            x: 0, y: 0,
            width: 20,
            name: "push",
            class: "push",
            min: false, max: false,
        });

        Object.assign(options, userOptions);

        super(options);

        let nameText = new Text({
            x: -options.radius,
            y: options.radius + 5
        });

        this.add(nameText);

        this.value = false;

        let buttonShape = new Rectangle();
        let valueShape = new Rectangle();
        this.add(valueShape);
        this.add(buttonShape);
        valueShape.setAttributes({"fill": "none"});
        valueShape.addClass("knob-value-arc");

        const remakeValueShape = () => {
            valueShape.attributes.width = options.width - 6;
            valueShape.attributes.height = options.width - 6;

            valueShape.attributes.x = 3 - options.width/2;
            if (this.value) {
                valueShape.attributes.y = 3 - options.width;
            } else {
                valueShape.attributes.y = 3;
            }

            valueShape.update();
        }

        const remakePath = () => {
            buttonShape.attributes.width = options.width;
            buttonShape.attributes.height = options.width * 2;

            buttonShape.attributes.x = -options.width/2;
            buttonShape.attributes.y = -options.width;

            nameText.attributes["text-anchor"]="middle";
            nameText.attributes.y = options.width * 1.5;

            nameText.update();
            buttonShape.update();
            remakeValueShape();

            nameText.setAttributes({"text": options.name});
        }

        remakePath();

        const pushCallbacks = [];

        const clickable = new Clickable(buttonShape.domElement);

        clickable.mouseDownCallback = () => {
            pushCallbacks.forEach(c=>c());
            this.value=true;
            remakePath();
        }
        clickable.mouseUpCallback = () => {
            this.value=false;
            remakePath();
        }

        /** @param {Function} cb */
        this.onPush = (cb) => {
            pushCallbacks.push(cb);
        }


    }
}

export default Button;