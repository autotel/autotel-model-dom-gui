export default class Voice {
    isBusy = false;
    trigger = (p) => { }
    /** @type {AudioNode} */
    output;
    /** @param {AudioContext} audioContext */
    constructor(audioContext){}
    /** @param {AudioNode} to */
    connect = to => {
        this.output.connect(to);
    }
}