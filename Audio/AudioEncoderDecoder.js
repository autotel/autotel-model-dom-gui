
/**
 * @param {string} base64
 * @param {AudioContext} audioContext 
 * @returns {Promise<AudioBuffer>} 
 * */
const decodeBase64Audio = (base64, audioContext) => {
    var binary = window.atob(base64);
    var buffer = new ArrayBuffer(binary.length);
    var bytes = new Uint8Array(buffer);
    for (var i = 0; i < buffer.byteLength; i++) {
        bytes[i] = binary.charCodeAt(i) & 0xFF;
    }
    return new Promise((resolve, reject) => {
        audioContext.decodeAudioData(
            buffer,
            (audioBuffer) => {
                resolve(audioBuffer);
            },
            reject
        );
    });
}

export default class AudioEncoderDecoder {
    playbackRate = 1;
    /** @param {AudioContext} audioContext */
    constructor(audioContext) {
        /**
         * @type {AudioBuffer | null}
         */
        let audioBuffer = null;
        /** @param {String} url */
        this.loadSampleUrl = async (url) => {
            // untested
            var request = new XMLHttpRequest();
            request.open('GET', url, true);
            request.responseType = 'arraybuffer';
            try {
                const audioData = await new Promise((resolve, fail) => {
                    request.onload = () => {
                        resolve(request.response);
                    }
                    request.onerror = fail;
                    request.send();
                });
                try {
                    audioBuffer = await audioContext.decodeAudioData(audioData);
                } catch (e) {
                    console.error(audioData, e);
                    throw new Error("could not decode audio data");
                }
            } catch (e) {
                console.error(url, e);
                throw new Error("could not get sample from url " + url);
            }
        }
        /**
         * @callback SynthesisFunction
         * @argument {number} sampleNo
         * @argument {number} channelNo 
         * @returns {number} 
         */
        /**
         * @param {SynthesisFunction} synthesisFunction function that returns the value for each sample
         * @param {number} lengthSpls how many samples to produce
         * @param {number} channels how many channels 
         * */
        this.synthesizeSample = async (synthesisFunction, lengthSpls, channels) => {
            // untested
            var newArrayBuffer = audioContext.createBuffer(channels, lengthSpls, audioContext.sampleRate);
            for (var channelNo = 0; channelNo < newArrayBuffer.numberOfChannels; channelNo++) {
                var nowBuffering = newArrayBuffer.getChannelData(channelNo);
                for (var sampleNo = 0; sampleNo < newArrayBuffer.length; sampleNo++) {
                    nowBuffering[sampleNo] = synthesisFunction(sampleNo, channelNo);
                }
            }
            audioBuffer = newArrayBuffer;
        }
        this.setSampleToWavUint8 = async (uint8Sample) => {
            // untested
            audioBuffer = await audioContext.decodeAudioData(uint8Sample);

        }
        /** @param {String} base64 */
        this.loadSampleBase64 = async (base64) => {
            // untested
            audioBuffer = await decodeBase64Audio(base64, audioContext);
        }
        this.getAudioSource = ()=>{
            const audioSource = audioContext.createBufferSource();
            audioSource.buffer = audioBuffer;
            return audioSource;
        }
        this.getAudioBuffer = ()=>{
            return audioBuffer;
        }
    }
}


