class MidiGetter {
    constructor() {
        let alreadyWiredInputs = new Set();
        let alreadyWiredOutputs = new Set();
        if(MidiGetter.instance) console.warn("creating a new instance of MidiGetter");
        MidiGetter.instance = this;
        /**
         * @typedef {Object} getReturn
         * @property {Set<MidiInput>} getReturn.inputs 
         * @property {Set<MidiOutput>} getReturn.outputs
         */
        /** 
         * @returns {Promise<getReturn>}
         **/
        this.get = async (inputNamed=false,outputNamed=false) => {
            // request MIDI access
            if (navigator.requestMIDIAccess) {
                let midiAccess = await navigator.requestMIDIAccess({
                    sysex: false
                });
                let inputs = midiAccess.inputs.values();
                let outputs = midiAccess.inputs.values();
                console.log(inputs);
                
                for(let input = inputs.next(); input && !input.done; input = inputs.next()){
                    if(
                        (
                            inputNamed == input.value.name || 
                            inputNamed===false
                        ) && !alreadyWiredInputs.has(input.value.id)
                    ){
                        console.log(input);
                        new MidiInput(input);
                        alreadyWiredInputs.add(input.value.id);
                    }
                }
                for(let output = outputs.next(); output && !output.done; output = outputs.next()){
                    if(
                        (
                            outputNamed == output.value.name || 
                            outputNamed===false
                        ) && !alreadyWiredOutputs.has(output)
                    ){
                        console.log(output);
                        new MidiOutput(output);
                        alreadyWiredOutputs.add(output);
                    }
                }
                return {
                    inputs:MidiInput.list,
                    outputs:MidiOutput.list,
                };

            } else {
                throw new Error("No MIDI support in your browser.");
            }
        }
        /**
         * @typedef {Object} listReturn
         * @property {Array<string>} getReturn.inputs 
         * @property {Array<string>} getReturn.outputs
         */
        /** 
         * @returns {Promise<listReturn>}
         **/
        this.list = async () => {
            // request MIDI access
            if (navigator.requestMIDIAccess) {
                let midiAccess = await navigator.requestMIDIAccess({
                    // sysex: false
                });
                let inputs = midiAccess.inputs.values();
                let outputs = midiAccess.inputs.values();
                console.log(inputs);

                let ret = {
                    inputs:[],
                    outputs:[],
                };
                
                for(let input = inputs.next(); input && !input.done; input = inputs.next()){
                    console.log({input});
                    ret.inputs.push(input.value.name);
                }

                for(let output = outputs.next(); output && !output.done; output = outputs.next()){
                    console.log({output});
                    ret.outputs.push(output.value.name);
                }

                return ret;

            } else {
                throw new Error("No MIDI support in your browser.");
            }
        }
    }   
}

/** @type {MidiGetter|false} */
MidiGetter.instance = false;
MidiGetter.getOrCreate = () => MidiGetter.instance?MidiGetter.instance:new MidiGetter();

/**
 * @callback midiInCallback
 * @param {Object} message
 * @param {Array<number>} message.data
 */

class MidiInput {
    constructor(midiInput){
        this.midiInput = midiInput;
        let messageListeners = [];
        /** @param {midiInCallback} callback */ 
        this.onMidiMessage = (callback) => {
            messageListeners.push(callback);
        }
        
        midiInput.value.onmidimessage = (evt) => {
            console.log({evt});
            messageListeners.forEach(l=>l(evt));
        }

        MidiInput.list.add(this);
    }
}

MidiInput.list = new Set();
/** unfinished! */
class MidiOutput {
    constructor(midiOutput){
        this.midiOutput = midiOutput;
        MidiOutput.list.add(this);
    }
}

MidiOutput.list = new Set();


export { MidiGetter, MidiInput, MidiOutput }