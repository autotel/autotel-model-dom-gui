import Voice from "./Voice";
/**
 * @typedef {Class} VoiceConstructor
 * @property {boolean} VoiceConstructor.isBusy
 **/

export default class PolyManager {
    maxVoices = 32;
    /** @type {Array<Voice>} */
    list = [];
    lastStolenVoice = 0;
    /**
     * @param {VoiceConstructor} VoiceConstructor 
     * @param {Object} voiceParams 
     **/
    constructor(VoiceConstructor,voiceParams = {}) {
        this.getVoice = () => {
            let found = null;
            this.list.forEach(voice => {
                if (!voice.isBusy) {
                    found = voice;
                }
            });
            if (!found) {
                if (this.list.length > this.maxVoices) {
                    found = this.list[this.lastStolenVoice];
                    this.lastStolenVoice += 1;
                    this.lastStolenVoice %= this.maxVoices;
                } else {
                    found = new VoiceConstructor(voiceParams);
                    this.list.push(found);
                }
            }
            return found;
        }
    }
}