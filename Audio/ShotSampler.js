import AudioEncoderDecoder from "./AudioEncoderDecoder";
import Voice from "./Voice";

/**
 * @param {string} base64
 * @param {AudioContext} audioContext 
 * @returns {Promise<AudioBuffer>} 
 * */
const decodeBase64Audio = (base64, audioContext) => {
    var binary = window.atob(base64);
    var buffer = new ArrayBuffer(binary.length);
    var bytes = new Uint8Array(buffer);
    for (var i = 0; i < buffer.byteLength; i++) {
        bytes[i] = binary.charCodeAt(i) & 0xFF;
    }
    return new Promise((resolve, reject) => {
        audioContext.decodeAudioData(
            buffer,
            (audioBuffer) => {
                resolve(audioBuffer);
            },
            reject
        );
    });
}

// TODO: remove own functions that are covered in AudioEncoderDecoder, and add an own property containing an AudioDecoderEncoder.
export default class ShotSampler extends Voice {
    playbackRate = 1;
    /** @param {AudioContext} audioContext */
    constructor(audioContext) {
        super(audioContext);
        let audioDecoder = new AudioEncoderDecoder(audioContext);
        /**
         * @type {AudioBuffer | null}
         */
        let audioBuffer = null;
        /** @type {AudioBufferSourceNode} */
        let audioSource;

        /** @type {GainNode} */
        let myGainNode = audioContext.createGain();
        myGainNode.gain.value = 0.6;
        /** @type {GainNode} */
        this.output = myGainNode;
        /** @param {AudioEncoderDecoder} newAudioDecoder*/
        this.setAudioDecoder = (newAudioDecoder) => {
            audioDecoder = newAudioDecoder;
            audioBuffer = audioDecoder.getAudioBuffer();
        }
        /** @param {String} url */
        this.loadSampleUrl = async (url) => {
            await audioDecoder.loadSampleUrl(url);
            audioBuffer = audioDecoder.getAudioBuffer();
        }
        /**
         * @callback SynthesisFunction
         * @argument {number} sampleNo
         * @argument {number} channelNo 
         * @returns {number} 
         */
        /**
         * @param {SynthesisFunction} synthesisFunction function that returns the value for each sample
         * @param {number} lengthSpls how many samples to produce
         * @param {number} channels how many channels 
         * */
        this.synthesizeSample = async (synthesisFunction, lengthSpls, channels) => {
            await audioDecoder.synthesizeSample(synthesisFunction, lengthSpls, channels);
            audioBuffer = audioDecoder.getAudioBuffer();
        }
        this.setSampleToWavUint8 = async (uint8Sample) => {
            await audioDecoder.setSampleToWavUint8(uint8Sample);
            audioBuffer = audioDecoder.getAudioBuffer();
        }
        /** @param {String} base64 */
        this.loadSampleBase64 = async (base64) => {
            await audioDecoder.loadSampleBase64(base64);
            audioBuffer = audioDecoder.getAudioBuffer();
        }
        /** @param {AudioBuffer} newAudioBuffer */
        this.setAudioBuffer = (newAudioBuffer) => {
            audioBuffer = newAudioBuffer;
        }

        this.trigger = (when) => {
            // untested
            if (!audioBuffer) throw new Error("you need to provide an audiobuffer before trying to trigger sample.");
            if (audioSource) audioSource.disconnect();
            audioSource = audioContext.createBufferSource();
            audioSource.buffer = audioBuffer;
            audioSource.playbackRate.value = this.playbackRate;
            audioSource.connect(this.output);
            audioSource.onended = () => {
                this.isBusy = false;
                audioSource.onended = null;
            }
            this.isBusy = true;
            if (when) return audioSource.start(when);
            return audioSource.start(when);
        }

        this.stop = (when) => {
            this.isBusy = false;
            if (!audioSource) return;
            if (when) return audioSource.stop(when);
            return audioSource.stop();
        }

    }
}


