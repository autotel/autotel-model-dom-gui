import { Div, TextField, P, Ul } from "../GuiComponents/DOMElements";
import {ListElement, DOMElementsArray} from "../GuiComponents/ElementsArray";
import Model from "../Model";

//create some abstractions, based on model or on gui elements
// of course that each class would go in its own file in a meaningful folder structure

/** a model that contains a succession of numbers based on some math functions */
class NumbersList extends Model {
    constructor(){
        /** settings that the model is going to pseudo-watch */
        const settings = {
            linear:0,
            quadratic:0,
            logarithmic:0,
            listSize:10,
            baseNumber:0,
            list:[],
        };

        super(settings);
        /** create a new list of numbers, for when the formula constants are changed */
        const remakeList=()=>{
            for(let i=0; i<settings.listSize; i++){

                settings.list[i] = settings.baseNumber
                    + i * settings.linear
                    + Math.pow(i,settings.quadratic)
                    + Math.pow(settings.logarithmic,i);

                console.log(this.settings.logarithmic);

            }
            //this is like "set" but does not set the value.
            //I use it here because I have already set the value manually
            this.changed({list:settings.list});
        }
        /** setup change listeners */
        this.onUpdate((changes)=>{
            //these ifs let us prevent infinite recursion:
            //since changes in constant cause changes in list, 
            //we dont want to remake the list if the list changes
            if(
                changes.linear!==undefined || 
                changes.quadratic!==undefined || 
                changes.logarithmic!==undefined ||
                changes.listSize!==undefined || 
                changes.baseNumber!==undefined
            ){
                remakeList();
            }
        });
        
    }
}
/** class that treats a text input as a model of sorts, allowing listeners when user inputs */
class InputModel extends Model {
    constructor(inputSettings){
        const settings = {
            value:0,
            controls:inputSettings.controls,
        };
        super(settings);

        const valChanged=()=>{
            this.set({
                value:parseFloat(this.textElement.domElement.value)||0,
            });
        }

        this.textElement = new TextField({
            placeholder:"set"+inputSettings.controls
        });
        this.textElement.domElement.addEventListener("input",valChanged);
    }
}

class NumberDisplay extends ListElement {
    constructor (){
        super();
        this.domElement=document.createElement("li");
        this.set=(number)=>{
            this.domElement.innerHTML=JSON.stringify(number);
        }
    }
}

// instantiate our abstractions

const listComponent = new Ul();

const mainContainer = new Div();

const numbersList = new NumbersList();
window.numbersList = numbersList;

const numbersListDisplay = new DOMElementsArray(NumberDisplay,listComponent,{});


const baseNumberOutput = new InputModel({controls:"baseNumber"});
const linearInput = new InputModel({controls:"linear"});
const quadraticInput = new InputModel({controls:"quadratic"});
const logarithmicInput = new InputModel({controls:"logarithmic"});

//append elements

document.body.appendChild(mainContainer.domElement);
mainContainer.add(
    baseNumberOutput.textElement,
    linearInput.textElement,
    quadraticInput.textElement,
    logarithmicInput.textElement,
    listComponent,
);

//create listeners, patching causes to consequences

numbersList.onUpdate((changes)=>{
    if(changes.list){
        numbersListDisplay.displayArray(
            numbersList.settings.list
        );
    }
});

[
    baseNumberOutput,
    linearInput,
    quadraticInput,
    logarithmicInput,
].forEach((el)=>el.onUpdate((changes)=>{

    if(changes.value!==undefined){
        const setObject = {}
        setObject[el.settings.controls] = el.settings.value;

        console.log(
            el.settings.controls,
            changes,
            setObject
        );

        numbersList.set(setObject);
    }
}));
